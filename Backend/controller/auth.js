import express from "express";
import pool from "../db.js";
import responseModel from "../utility/baseModel.js";

const router = express.Router();

router.post("/signup", async (req, resp) => {
  try {
    const { email, password } = req.body;
    const statement = `INSERT INTO userdetails (email,password) VALUES (?,?)`;

    const data = await pool.execute(statement, [email, password]);
    if (data[0].affectedRows != 0) {
      resp.status(200).send(responseModel(true, data[0]));
    } else {
      resp.status(503).send(responseModel(false, "fail to sign up"));
    }
  } catch (e) {
    resp.status(500).send(responseModel(false, e.message));
  }
});

router.post("/login", async (req, resp) => {
  try {
    const { email, password } = req.body;
    const statement = `SELECT id, email FROM userdetails WHERE email=? and password=?`;

    const data = await pool.execute(statement, [email, password]);

    if (data[0].length != 0) {
      req.session.user = await data[0];
      resp.status(200).send(responseModel(true, data[0]));
    } else {
      resp.status(401).send(responseModel(false, "invalid credentials"));
    }
  } catch (e) {
    resp.status(500).send(responseModel(false, e.message));
  }
});

router.get("/logout", async (req, resp) => {
  try {
    if (req.session.user) {
      req.session.destroy();
      resp.clearCookie("session");
      resp
        .status(200)
        .send(responseModel(true, "user logged out successfully"));
    } else {
      resp.status(400).send(responseModel(false, "no session found"));
    }
  } catch (e) {
    resp.status(500).send(responseModel(false, e.message));
  }
});

router.post("/resetPassward", async (req, resp) => {
  try {
    if (req.session.user) {
      const { oldPassword, newPassword } = req.body;
      const statement = `UPDATE userDetails
        SET password = ? WHERE email=? and password = ?`;
      const data = await pool.execute(statement, [
        newPassword,
        req.session.user[0].email,
        oldPassword,
      ]);
      if (data[0].affectedRows != 0) {
        resp
          .status(200)
          .send(responseModel(true, "password updated successfully"));
      } else {
        resp.status(401).send(responseModel(false, "invalid old password"));
      }
    } else {
      resp.status(401).send(responseModel(false, "invalid session"));
    }
  } catch (e) {
    resp.status(500).send(responseModel(false, e.message));
  }
});

export default router;
