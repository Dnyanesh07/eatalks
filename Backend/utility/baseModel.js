import { response } from "express"

const responseModel=(status,data)=>{
    const response={}
    response['status']=status;
    response['data']=data;
    return response;
}

export default responseModel;