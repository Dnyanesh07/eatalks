import express from "express";
import pool from "../db.js";
import responseModel from "../utility/baseModel.js";

const router = express.Router();

router.get("/profile", async (req, resp) => {
  if (req.session.user) {
    const statement = `SELECT * FROM primarydetails WHERE userDetailsId = ?`;
    const a = req.session.user[0].id;
    const data = await pool.execute(statement, [a]);
    if (data[0].length != 0) {
      resp.status(200).send(responseModel(true, data[0]));
    } else {
      resp.status(400).send(responseModel(false, "no profile details found"));
    }
  } else {
    resp.status(401).send(responseModel(false, "invalid session"));
  }
});

router.post("/add", async (req, resp) => {
  if (req.session.user) {
    const { name, bio, imageURL, mobile, gender } = req.body;
    const statement = `INSERT INTO primarydetails (userDetailsId, name, bio, imageURL, mobile, gender) VALUE (?,?,?,?,?,?)`;
    const a = req.session.user[0].id;
    const data = await pool.execute(statement, [
      req.session.user[0].id,
      name,
      bio,
      imageURL,
      mobile,
      gender,
    ]);
    if (data[0].affectedRows != 0) {
      resp.status(200).send(responseModel(true, data[0]));
    } else {
      resp.status(503).send(responseModel(false, "Error while adding user"));
    }
  } else {
    resp.status(401).send(responseModel(false, "Invalid session"));
  }
});

router.post("/update", async (req, resp) => {
  if (req.session.user) {
    const { name, bio, imageURL, mobile } = req.body;
    const statement = `UPDATE primarydetails SET name=?, mobile=?, bio=?, imageurl=? WHERE userDetailsId = ?`;
    const data = await pool.execute(statement, [
      name,
      mobile,
      bio,
      imageURL,
      req.session.user[0].id,
    ]);
    if (data[0].affectedRows != 0) {
      resp.status(200).send(responseModel(true, "Profile updated successfully"));
    } else {
      resp.status(400).send(responseModel(false, "No profile details found"));
    }
  } else {
    resp.status(401).send(responseModel(false, "Invalid session"));
  }
});

export default router;
