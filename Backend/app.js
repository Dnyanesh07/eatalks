import express from "express";
import bodyParser from "body-parser";
import cookieParser from "cookie-parser";
import expressSession from "express-session";

import blogRouter from "./controller/blog.js";
import authRouter from "./controller/auth.js";
import userRouter from "./controller/user.js";

const app = express();

app.use(bodyParser.json({ type: "application/json" }));
//bodyParser.json();

app.use(
  expressSession({
    key: "session",
    secret: "HIRT_Project_Secret",
    resave: false,
    saveUninitialized: false,
    cookie: {
      maxAge: 1000 * 60 * 60 * 24 * 7, // (maxAge : one week)
    },
  })
);

app.use(cookieParser());

app.use("/auth", authRouter);
app.use("/user", userRouter);
app.use("/blog", blogRouter);

app.get("/", (req, res) => {
  res.send("Welcome to EaTalks");
});

app.listen(4000, () => {
  console.log("server is running on port 4000");
});
