import express from "express";
import pool from "../db.js";
import responseModel from "../utility/baseModel.js";
import mysql from "mysql2";

const router = express.Router();

router.get("/getblogs", async (req, resp) => {
  try {
    const statement = `SELECT * FROM blog ORDER BY datetime DESC limit 20`;
    const data = await pool.execute(statement);
    if (data[0].length > 0) {
      resp.status(200).send(responseModel(true, data[0]));
    } else {
      resp.status(200).send(responseModel(false, "No blogs available"));
    }
  } catch (e) {
    resp.status(500).send(responseModel(false, e.message));
  }
});

router.post("/getBlogsByUserId", async (req, resp) => {
  try {
    if (req.session.user) {
      const statement = `SELECT * FROM blog WHERE userDetailsId = ? ORDER BY datetime DESC`;
      const data = await pool.execute(statement, [req.session.user[0].id]);
      if (data[0].length > 0) {
        resp.status(200).send(responseModel(true, data[0]));
      } else {
        resp.status(200).send(responseModel(false, "No blog available"));
      }
    } else {
      resp.status(401).send(responseModel(false, "Invalid session"));
    }
  } catch (e) {
    resp.status(500).send(responseModel(false, e.message));
  }
});

router.post("/getBlogsByTitle/:title", async (req, resp) => {
  try {
    if (req.session.user) {
      const { title } = req.params;
      const statement =
        "SELECT * FROM blog WHERE title LIKE ? ORDER BY datetime DESC";
      const data = await pool.execute(statement, [`%${title}%`]);
      if (data[0].length > 0) {
        resp.status(200).send(responseModel(true, data[0]));
      } else {
        resp.status(200).send(responseModel(false, "No blog available"));
      }
    } else {
      resp.status(401).send(responseModel(false, "Invalid session"));
    }
  } catch (e) {
    resp.status(500).send(responseModel(false, e.message));
  }
});

router.post("/getBlogsByTag/:tag", async (req, resp) => {
  try {
    if (req.session.user) {
      const { tag } = req.params;
      const statement =
        "SELECT * FROM blog WHERE tags LIKE ? ORDER BY datetime DESC";
      const data = await pool.execute(statement, [`%${tag}%`]);
      if (data[0].length > 0) {
        resp.status(200).send(responseModel(true, data[0]));
      } else {
        resp.status(200).send(responseModel(false, "No blog available"));
      }
    } else {
      resp.status(401).send(responseModel(false, "Invalid session"));
    }
  } catch (e) {
    resp.status(500).send(responseModel(false, e.message));
  }
});

router.get("/getComponents/:blogId", async (req, resp) => {
  try {
    const { blogId } = req.params;
    const statement = `SELECT * FROM subblog WHERE blogId = ? ORDER BY indexId`;
    const data = await pool.execute(statement, [blogId]);
    if (data[0].length > 0) {
      resp.status(200).send(responseModel(true, data[0]));
    } else {
      resp
        .status(200)
        .send(responseModel(false, "No blog components available"));
    }
  } catch (e) {
    resp.status(500).send(responseModel(false, e.message));
  }
});

router.post("/addBlog", async (req, resp) => {
  try {
    if (req.session.user) {
      const { title, authorName, shortDesc, imageURL, category, tags } =
        req.body;
      const statement = `INSERT INTO blog (userDetailsId,title,authorName,shortDesc,imageURL,category,tags) VALUES (?,?,?,?,?,?,?)`;
      const data = await pool.execute(statement, [
        req.session.user[0].id,
        title,
        authorName,
        shortDesc,
        imageURL,
        category,
        tags,
      ]);
      if (data[0].affectedRows != 0) {
        resp.status(200).send(responseModel(true, "Blog added successfully"));
      } else {
        resp.status(503).send(responseModel(false, "Error while adding blog"));
      }
    } else {
      resp.status(401).send(responseModel(false, "Invalid session"));
    }
  } catch (e) {
    resp.status(500).send(responseModel(false, e.message));
  }
});

router.post("/updateBlog", async (req, resp) => {
  try {
    if (req.session.user) {
      const { title, shortDesc, imageURL, category, tags, id } = req.body;
      const statement = `UPDATE blog SET title = ?,shortDesc = ?,imageURL = ?,category = ?,tags = ? WHERE id = ?`;
      const data = await pool.execute(statement, [
        title,
        shortDesc,
        imageURL,
        category,
        tags,
        id,
      ]);
      if (data[0].affectedRows != 0) {
        resp.status(200).send(responseModel(true, "Blog updated successfully"));
      } else {
        resp
          .status(503)
          .send(responseModel(false, "Error while updating blog"));
      }
    } else {
      resp.status(401).send(responseModel(false, "Invalid session"));
    }
  } catch (e) {
    resp.status(500).send(responseModel(false, e.message));
  }
});

router.post("/deleteBlog", async (req, resp) => {
  try {
    if (req.session.user) {
      const { id } = req.body;
      const statement1 = `DELETE FROM blog WHERE id = ?`;
      const data = await pool.execute(statement1, [id]);
      if (data[0].affectedRows != 0) {
        resp.status(200).send(responseModel(true, "Blog deleted successfully"));
      } else {
        resp
          .status(503)
          .send(responseModel(false, "Error while deleting blog"));
      }
    } else {
      resp.status(401).send(responseModel(false, "Invalid session"));
    }
  } catch (e) {
    resp.status(500).send(responseModel(false, e.message));
  }
});

router.post("/deleteComponent", async (req, resp) => {
  try {
    if (req.session.user) {
      const { blogId } = req.body;
      const statement = `DELETE FROM subblog WHERE blogId = ?`;
      const data = await pool.execute(statement, [blogId]);
      if (data[0].affectedRows != 0) {
        resp
          .status(200)
          .send(responseModel(true, "Blog component deleted successfully"));
      } else {
        resp
          .status(503)
          .send(responseModel(false, "Error while deleting blog component"));
      }
    } else {
      resp.status(401).send(responseModel(false, "Invalid session"));
    }
  } catch (e) {
    resp.status(500).send(responseModel(false, e.message));
  }
});

router.post("/addBlogComponent", async (req, resp) => {
  try {
    if (req.session.user) {
      const { jsonArray } = req.body;
      const sampleObject = jsonArray[0];
      const columns = Object.keys(sampleObject);

      const values = jsonArray.map((jsonObj) =>
        columns.map((column) => mysql.escape(jsonObj[column]))
      );

      const query = `INSERT INTO subblog (${columns.join(", ")}) VALUES ${values
        .map((row) => `(${row.join(", ")})`)
        .join(", ")}`;
      const data = await pool.execute(query);
      if (data[0].affectedRows != 0) {
        resp
          .status(200)
          .send(responseModel(false, "Blog components added succesfully"));
      } else {
        resp
          .status(503)
          .send(responseModel(false, "Error while adding component"));
      }
    } else {
      resp.status(401).send(responseModel(false, "Invalid session"));
    }
  } catch (e) {
    resp.status(500).send(responseModel(false, e.message));
  }
});

router.post("/updateComponent", async (req, resp) => {
  try {
    if (req.session.user) {
      const { subTitle, imageURL, content, blogId } = req.body;
      const statement = `UPDATE subblog SET subTitle = ?,content = ?,imageURL = ? WHERE blogId = ?`;
      const data = await pool.execute(statement, [
        subTitle,
        content,
        imageURL,
        blogId,
      ]);
      if (data[0].affectedRows != 0) {
        resp
          .status(200)
          .send(responseModel(true, "Component updated successfully"));
      } else {
        resp
          .status(503)
          .send(responseModel(false, "Error while updating component"));
      }
    } else {
      resp.status(401).send(responseModel(false, "Invalid session"));
    }
  } catch (e) {
    resp.status(500).send(responseModel(false, e.message));
  }
});

export default router;
